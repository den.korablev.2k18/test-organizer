export const environment: any = {
  production: false,
  baseApiUrl: 'http://localhost:5235/api'
}

export enum ApiPath {
  // Registration = '/account/register',
  // Login = '/account/login',
  // Notes = '/notes',
  Tag = '/Tag',
  Notification = '/Notification',
  // NoteTag = '/tags/notetag'
}
