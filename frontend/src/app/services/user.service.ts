import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../interfaces/user';
import { AuthUser } from '../interfaces/AuthUser';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  url = 'http://localhost:5235/api/User'
  token?: string;
  userId?: string;
  constructor(private _http: HttpClient) { }
  inizializeUser(user: User) {
    return this._http.post<AuthUser>(`${this.url}/InizializeUser`, user);
  }
}
