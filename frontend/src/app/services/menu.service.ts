import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Menu } from '../interfaces/menu';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  public menu: Menu = {
    tag: true,
    notification: false
  }

  public $menuSubject: BehaviorSubject<Menu> = new BehaviorSubject(this.menu);
  constructor() { }

  public changeMenu(menu: Menu): void {
    this.$menuSubject.next(menu);
  }
}
