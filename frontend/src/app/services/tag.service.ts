import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiPath, environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  private url = environment.baseApiUrl + ApiPath.Tag;
  constructor(private http: HttpClient) { }
  createTag(body: unknown) {
    return this.http.post<unknown>(`${this.url}/CreateTag`, body)
  }
}
