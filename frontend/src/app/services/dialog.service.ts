import { Injectable, Type } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

@Injectable({
  providedIn: 'root',
})
export class DialogService {

  constructor(private _dialog: MatDialog) { }
  openDialog<C, T>(component: Type<C>, params: T, maxWidth: string = '520px') {
    const dialogConfig = new MatDialogConfig<T>();

    dialogConfig.autoFocus = true;
    dialogConfig.data = params;
    dialogConfig.width = '100%';
    dialogConfig.maxWidth = maxWidth;
    dialogConfig.disableClose = true;

    const dialog = this._dialog.open(component, dialogConfig);
    return dialog.afterClosed();
  }
}
