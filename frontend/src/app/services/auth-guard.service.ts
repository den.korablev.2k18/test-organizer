import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { UserService } from './user.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private _router: Router, private _userService: UserService) { }
  canActivate(): boolean {
    if (this._userService.token) {
      return true;
    } else {
      this._router.navigate(['/']);
      return false;
    }
  }
}
