import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiPath, environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private url = environment.baseApiUrl + ApiPath.Notification;
  constructor(private http: HttpClient) { }

  createNotification(body: unknown) {
    return this.http.post(`${this.url}/CreateNotification`, body);
  }

}
