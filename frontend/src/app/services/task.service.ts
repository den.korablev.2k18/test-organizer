import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { CreateTask } from '../interfaces/create-task';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  private static URL = 'https://testorganizer-5ff0a-default-rtdb.firebaseio.com'
  constructor(private http: HttpClient) { }


  public read<T>(date: moment.Moment, type: string): Observable<T[]>{
    return this.http.get<T[]>(`${TaskService.URL}/${type}/${date.format('DD-MM-YYYY')}.json`)
      .pipe(map(tasks => {
        return tasks ? Object.keys(tasks).map((key:any) => ({...tasks[key], id: key })): [];
      }));
  }

  public create<T>(task: T, type: string, date:string): Observable<T> {
    return this.http.post<CreateTask>(`${TaskService.URL}/${type}/${date}.json`, task).pipe(map(res =>{
      return {...task,id: res.name}
    }))
  }

  public update<T>(task: T, type: string, date:string, id: string): Observable<T> {
    return this.http.put<CreateTask>(`${TaskService.URL}/${type}/${date}/${id}.json`, task).pipe(map(res =>{
      return {...task,id: res.name}
    }))
  }

  public delete(type:string, date: string, id: string): Observable<void> {
    return this.http.delete<void>(`${TaskService.URL}/${type}/${date}/${id}.json`)
  }
}
