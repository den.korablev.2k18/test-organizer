export type EntityUnion = Notification & Tag & Schedule;

type CommonEntity = {
  userID: string;
}

export type Tag = CommonEntity & {
  head: string;
  date: string;
};

export type Notification = CommonEntity & {
  deadline: string;
  title: string;
}

export type Schedule = CommonEntity & {
  title: string;
  date: string;
}
