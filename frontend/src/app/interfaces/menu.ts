export interface Menu {
    notification: boolean;
    tag: boolean;
}

export enum MenuList {
    NOTIFICATION = 'notification',
    TAG = 'tag'
}

export enum InizializeUser{
  Registration = 'Registration',
  ComeIn = 'ComeIn'
}
