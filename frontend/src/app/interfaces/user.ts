export interface User {
  login: string;
  password: string;
  isRegistration: boolean;
}
