export interface AuthUser{
  token: string;
  userId: string;
  errorMessage: string;
}
