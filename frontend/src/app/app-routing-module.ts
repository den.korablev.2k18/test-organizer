import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./services/auth-guard.service";
import { AdminPageComponent } from "./pages/admin-page/admin-page.component";
import { MainPageComponent } from "./pages/main-page/main-page.component";
const routes: Routes = [
  {path: "", component: MainPageComponent},

  {
    path:'admin', canActivate:[AuthGuard], component: AdminPageComponent
  }
]
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
