import { Component, OnInit } from '@angular/core';
import { DateService } from 'src/app/services/date.service';
import * as moment from 'moment';

@Component({
  selector: 'app-selector',
  templateUrl: './selector.component.html',
  styleUrls: ['./selector.component.scss']
})
export class SelectorComponent implements OnInit {

  public date!: moment.Moment;
  constructor(public dateService: DateService) { }

  ngOnInit(): void {
    this.dateService.date.subscribe((date: moment.Moment)=>{
      this.date = date;
    })
  }

  public setDate(dir:number):void{
    this.dateService.changeMonth(dir);
  }

}
