import { Component, NgModule, OnInit } from '@angular/core';
import { InizializeUser, Menu, MenuList } from 'src/app/interfaces/menu';
import { DialogService } from 'src/app/services/dialog.service';
import { MenuService } from 'src/app/services/menu.service';
import { InizializeUserComponent } from '../shared/inizialize-user/inizialize-user.component';
import { filter, map, switchMap } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { AuthUser } from 'src/app/interfaces/AuthUser';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  menu!: Menu;
  inizializeUser = InizializeUser;
  public MenuList = MenuList;
  constructor(private menuService:MenuService, private _dialogService: DialogService, private _userService: UserService, private _router: Router) { }

  ngOnInit(): void {
    this.menuService.$menuSubject.subscribe((result:Menu)=>{
      this.menu = result;
    })
  }

  public setMenu(val:string): void {
    this.menu = {
      notification: val === this.MenuList.NOTIFICATION,
      tag: val === this.MenuList.TAG
    }
    this.menuService.changeMenu(this.menu);
  }

  public getIsActive(value: string): boolean {
    for (let item in this.menu) {
      if (this.menu[item as keyof Menu]) {
        return item === value;
      }
    }
    return false;
  }

  public initizializeUser(isRegister: boolean) {
    this._dialogService.openDialog(InizializeUserComponent, isRegister ? this.inizializeUser.Registration : this.inizializeUser.ComeIn)
      .pipe(
        filter((result) => !!result),
        map((user) => {
          return {
            ...user, isRegistration:isRegister
          }
        }),
        switchMap((user) => this._userService.inizializeUser(user))
    ).subscribe((response: AuthUser) => {
      if (response.token.length > 0) {
        this._userService.token = response.token;
        this._userService.userId = response.userId;
      }
      })
  }

  goAdmin() {
    this._router.navigateByUrl('/admin')
  }
}
