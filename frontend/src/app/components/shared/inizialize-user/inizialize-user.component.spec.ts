import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InizializeUserComponent } from './inizialize-user.component';

describe('InizializeUserComponent', () => {
  let component: InizializeUserComponent;
  let fixture: ComponentFixture<InizializeUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InizializeUserComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(InizializeUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
