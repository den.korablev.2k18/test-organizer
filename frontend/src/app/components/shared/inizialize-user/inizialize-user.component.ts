import { Component,  Inject,  } from '@angular/core';
import {FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
const patternName = /^[A-Za-z0-9]{4,40}$/;
@Component({
  selector: 'app-inizialize-user',
  templateUrl: './inizialize-user.component.html',
  styleUrls: ['./inizialize-user.component.scss'],
})
export class InizializeUserComponent {
  form: FormGroup = new FormGroup({
    login: new FormControl(null, [Validators.required]),
    password: new FormControl(null, [Validators.required, Validators.pattern(patternName)]),
  })
  constructor(@Inject(MAT_DIALOG_DATA) public title: string) { }
}
