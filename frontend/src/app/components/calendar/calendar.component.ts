import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Day } from 'src/app/interfaces/day';
import { Week } from 'src/app/interfaces/week';
import { DateService } from 'src/app/services/date.service';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  public calendar?: Week[];
  public days: string[] = ['Sun', 'Mon', 'Tue', 'Wen', 'Thu', 'Fri', 'Sat'];
  constructor(public dateService: DateService) { }

  ngOnInit(): void {
    this.dateService.date.subscribe(this.generate.bind(this))
  }

  private generate(now:moment.Moment): void {
    const startDay = now.clone().startOf('month').startOf('week');
    const endDay = now.clone().endOf('month').endOf('week');

    const date = startDay.clone().subtract(1,'day');

    const calendar = [];

    while(date.isBefore(endDay,'day')){
      calendar.push({
        days: Array(7)
        .fill(0)
        .map(()=>{
          const value = date.add(1,'day').clone();
          const active = moment().isSame(value, 'date');
          const disable = !now.isSame(value, 'month');
          const select = now.isSame(value, 'date');
          let day: Day = {
            value: value,
            active: active,
            disabled: disable,
            selected: select
          }
          return day;
        })
      })
    }

    this.calendar = calendar;
  }

  public select(day: moment.Moment): void {
    this.dateService.changeDate(day);
  }

}
