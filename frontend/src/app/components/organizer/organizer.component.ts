import { Component, OnInit } from '@angular/core';
import { DateService } from 'src/app/services/date.service';
import * as moment from 'moment';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { TaskService } from 'src/app/services/task.service';
import { Observable, of, switchMap, tap } from 'rxjs';
import { MenuService } from 'src/app/services/menu.service';
import { Menu, MenuList } from 'src/app/interfaces/menu';
import { EntityUnion } from "../../interfaces/entity-union";
import { UserService } from 'src/app/services/user.service';
import { NotificationService } from 'src/app/services/notification.service';
import { TagService } from 'src/app/services/tag.service';

@Component({
  selector: 'app-organizer',
  templateUrl: './organizer.component.html',
  styleUrls: ['./organizer.component.scss']
})
export class OrganizerComponent implements OnInit {

  public date!: moment.Moment;
  public formSchedule!: FormGroup;
  public formNotification!: FormGroup;
  public formTag!: FormGroup;
  public tasks: Array<EntityUnion> = [];
  public isEdit: boolean = false;
  public selectedTask!: EntityUnion;
  public menu!: Menu;
  public MenuList = MenuList;
  constructor(public dateService: DateService, public taskService: TaskService, public menuService: MenuService, private _userService: UserService, private _notificationService: NotificationService, private _tagService: TagService) { }

  ngOnInit(): void {

    // this.formSchedule = new FormGroup({
    //   head: new FormControl('', Validators.required),
    //   title: new FormControl('', Validators.required),
    //   time: new FormControl('', Validators.required)
    // });

    this.formNotification = new FormGroup({
      userId: new FormControl(this._userService.userId ? this._userService.userId : null),
      title: new FormControl('', Validators.required),
      deadline: new FormControl(this.dateService.date.value.format('YYYY-MM-DD'), Validators.required),
      tag: new FormControl()
    });

    this.formTag = new FormGroup({
      userId: new FormControl(this._userService.userId ? this._userService.userId : null),
      head: new FormControl('', Validators.required),
      date: new FormControl(this.dateService.date.value.format('YYYY-MM-DD'), Validators.required),
      notification: new FormControl()
    })


    this.dateService.date
      .pipe(
        tap(date => this.date = date),
        switchMap(() => this.loopMenuList(this.date))
      )
      .subscribe((result: EntityUnion[])=> {
        this.tasks = result;
    });

    this.menuService.$menuSubject.subscribe((result: Menu) => {
      this.menu = result;
      this.loopMenuList(this.date).subscribe((res: EntityUnion[]) => {
        this.tasks = res;
      })
    });
  }

  public loopMenuList(value: moment.Moment): Observable<EntityUnion[]> {
    for (let item in this.menu) {
      if (this.menu[item as keyof Menu]) {
        return this.taskService.read<EntityUnion>(value, item)
          .pipe(
            tap((value) =>  this.tasks = value)
          )
      }
    }
    return of([]);
  }

  public submit(form: FormGroup, type: string): void {
    // form.value.date = this.dateService.date.value.format('DD-MM-YYYY')

    // this.taskService.create<EntityUnion>(form.value, type, form.value.date).subscribe((task) => {
    //   this.tasks.push(task)
    //   form.reset();
    // })

  }
  public edit(task: EntityUnion): void {
    // this.isEdit = true;
    // this.selectedTask = task;
    //  if(this.menu.notification){
    //   this.formNotification.setValue({head: task.head, title: task.title, deadline: task.deadline})
    // } else if(this.menu.tag){
    //   this.formTag.setValue({head: task.head})
    // }
  }
  saveNotification() {
    this.formNotification.get('userId')?.setValue(this._userService.userId);
    this.formTag.get('userId')?.setValue(this._userService.userId);
    this.formTag.removeControl('notification');
    this.formNotification.get('tag')?.setValue([this.formTag.getRawValue()]);
    this._notificationService.createNotification(this.formNotification.value).subscribe((result) => {
      console.error(result)
    })
  }
  saveTag() {
    this.formNotification.get('userId')?.setValue(this._userService.userId);
    this.formTag.get('userId')?.setValue(this._userService.userId);
    this.formNotification.removeControl('tag');
    this.formTag.get('notification')?.setValue([this.formNotification.getRawValue()]);
    this._tagService.createTag(this.formTag.value).subscribe((result) => {
      console.error(result)
    })
  }

  public save(type: string, form: FormGroup): void {
    // this.selectedTask.head = form.value.head;
    // this.selectedTask.title = form.value.title;


    // if(this.menu.notification){
    //   this.selectedTask.deadline = this.formNotification.value.deadline;
    // }

    // this.taskService.update<EntityUnion>(this.selectedTask, type, this.selectedTask.date, this.selectedTask.id).subscribe((task)=>{
    //   form.reset();
    // });

    // this.isEdit = false;
  }
  public remove(type: string, date: string, id: string): void {
    // this.taskService.delete(type, date, id).subscribe(()=>{
    //   this.tasks = this.tasks.filter((t: EntityUnion)=> t.id !== id)
    // })
  }
}


