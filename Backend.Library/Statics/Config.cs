﻿using Backend.Library.Exceptions;
using Microsoft.Extensions.Configuration;
namespace Backend.Library.Statics;

public static class Config
{
    /// <summary>
    /// Имя конфигурационного файла
    /// </summary>
    private const string ConfigurationFileName = "appsettings.json";

    /// <summary>
    /// Протоколирование
    /// </summary>
    private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

    /// <summary>
    /// Конфигурация
    /// </summary>
    private static readonly IConfigurationRoot config;

    /// <summary>
    /// Чтение конфигурации
    /// </summary>
    static Config()
    {
        string name = Path.Combine(AppContext.BaseDirectory, ConfigurationFileName);
        if (!File.Exists(name))
        {
            throw new AppException($"Файл конфигурации не найден: {name}");
        }
        config = new ConfigurationBuilder().AddJsonFile(name, optional: false).Build();
        log.Info($"Загружен файл конфигурации: {name}");
    }

    /// <summary>
    /// Чтение секции конфигурации в виде строки
    /// </summary>
    /// <param name="name">Имя параметра конфигурации</param>
    /// <returns></returns>
    public static string GetString(string name)
    {
        string? s = config.GetSection(name).Value;
        if (s == null)
        {
            log.Warn($"Не задан параметр конфигурации: {name}");
            s = string.Empty;
        }
        return s;
    }

    /// <summary>
    /// Чтение секции в виде объекта
    /// <para>Имя секции должно соответствовать имени класса</para>
    /// </summary>
    /// <typeparam name="T">Раздел конфигурации</typeparam>
    /// <param name="name">Имя параметра конфигурации</param>
    /// <param name="defaultValue">Значение по умолчанию, если параметр не задан в конфигурации</param>
    /// <returns>Экземпляр раздела конфигурации (пустой, если в файле нет требуемой секции)</returns>
    public static T? GetSection<T>(string name, T? defaultValue = default)
    {
        T? result = config.GetSection(name).Get<T>();
        if (result == null)
        {
            log.Warn($"В файле конфигурации отсутствует секция '{name}'");
            result = defaultValue;
        }
        return result;
    }
}
