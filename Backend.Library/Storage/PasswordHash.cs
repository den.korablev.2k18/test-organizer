﻿using System.Security.Cryptography;
using System.Text;

namespace Backend.Library.Storage;


public class PasswordHash : Entity
{
    /// <summary>
    /// Сырой пароль
    /// </summary>
    public virtual string PasswordRaw { get; set; }
    /// <summary>
    /// Зашифрованный пароль MD5
    /// </summary>
    public virtual string PassowrdHash { get; set; }
    /// <summary>
    /// Вычисление хеша
    /// </summary>
    /// <param name="plainText">Открытый текст</param>
    /// <returns></returns>
    public static string Hash(string plainText)
    {
        using var hasher = MD5.Create();
        string hash = Convert.ToHexString(hasher.ComputeHash(Encoding.UTF8.GetBytes(plainText)));
        return hash;
    }

    public bool inizializeTable()
    {

    return true; 
    }
}
