﻿using Backend.Library.Storage;

namespace Backend.Library.Storage;
/// <summary>
/// Тег
/// </summary>
public class Tag : Entity
{
    /// <summary>
    /// Заголовок тега
    /// </summary>
    public virtual string? Head { get; set; }
    /// <summary>
    /// Дата тега
    /// </summary>
    public virtual DateOnly? Date { get; set; }
    /// <summary>
    /// Guid пользователя
    /// </summary>
    public virtual Guid UserId { get; set; }
    /// <summary>
    /// теги пользователя
    /// </summary>
    public virtual User? User { get; set; }
    public virtual List<Notification> Notification { get; set; } = [];
}
