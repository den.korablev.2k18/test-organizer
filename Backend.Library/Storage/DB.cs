﻿using Backend.Library.Statics;
using Microsoft.EntityFrameworkCore;
using NLog;
using System.Reflection;

namespace Backend.Library.Storage;

public class DB : DbContext
{
    /// <summary>
    /// Протоколирование
    /// </summary>
    private static Logger log = LogManager.GetCurrentClassLogger();
    /// <summary>
    /// Уведомления
    /// </summary>
    public virtual DbSet<Notification> Notifications { get; set; }
    /// <summary>
    /// Теги
    /// </summary>
    public virtual DbSet<Tag> Tags { get; set; }
    /// <summary>
    /// Напоминания
    /// </summary>
    public virtual DbSet<Schedule> Schedules { get; set; }
    /// <summary>
    /// Пользователь
    /// </summary>
    public virtual DbSet<User> Users { get; set; } 
    public virtual DbSet<PasswordHash> PasswordHashes { get; set; }
    public DB() {
    }
    public DB(DbContextOptions<DB> options) : base(options) { }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        string cs = Config.GetSection<string>($"ConnectionStrings") ?? String.Empty;
        log.Info($"Строка соединения {cs}");
        string path = Path.Combine(AppContext.BaseDirectory, "Postgres.dll");
        string nameAssembly = AssemblyName.GetAssemblyName(path).ToString();
        log.Info($"Миграционная сборка {nameAssembly}");
        optionsBuilder.UseLazyLoadingProxies();
        optionsBuilder.UseNpgsql(cs, options =>
        {
            options.MigrationsAssembly(nameAssembly);
        });
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Notification>()
            .HasMany(e => e.Tag)
            .WithMany(e => e.Notification)
            .UsingEntity<NotificationTag>();

    }


}
