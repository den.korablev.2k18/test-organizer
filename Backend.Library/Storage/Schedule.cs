﻿namespace Backend.Library.Storage;
/// <summary>
/// Напоминания
/// </summary>
public class Schedule : Entity
{
    /// <summary>
    /// Время напоминания
    /// </summary>
    public virtual DateOnly? Date {  get; set; }
    /// <summary>
    /// Основная информация напоминания
    /// </summary>
    public virtual string? Title { get; set; }
    /// <summary>
    /// Guid Уведомления
    /// </summary>
    public virtual Guid NotificationId { get; set; }
}
