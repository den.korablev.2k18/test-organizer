﻿namespace Backend.Library.Storage;
/// <summary>
/// Кросс таблица для уведомлений и тегов
/// </summary>
public class NotificationTag : Entity
{
    /// <summary>
    /// Guid уведомления
    /// </summary>
    public virtual Guid? NotificationId { get; set; }
    /// <summary>
    /// Guid тега
    /// </summary>
    public virtual Guid? TagId { get; set; }
}
