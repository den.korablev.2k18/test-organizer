﻿namespace Backend.Library.Storage;
/// <summary>
/// Корневая сущность хранения с уникальный идентификатором
/// </summary>
public abstract class Entity
{
    /// <summary>
    /// Уникальный идентификатор
    /// </summary>
    public virtual Guid Id { get; set; } = Guid.NewGuid();
}

