﻿namespace Backend.Library.Storage;

public class User : Entity
{
    /// <summary>
    /// Логин пользователя
    /// </summary>
    public virtual string? Login { get; set; }


    /// <summary>
    /// Зашифрованный пароль MD5
    /// </summary>
    public virtual string? PasswordHash { get; set; }
}
