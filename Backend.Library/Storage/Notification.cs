﻿namespace Backend.Library.Storage;
/// <summary>
/// Уведомления
/// </summary>
public class Notification : Entity
{
    /// <summary>
    /// Основная информация напоминания
    /// </summary>
    public virtual string? Title { get; set; }
    /// <summary>
    /// Конец 
    /// </summary>
    public virtual DateOnly? Deadline { get; set; }
    public virtual Guid UserId { get; set; }
    /// <summary>
    /// теги пользователя
    /// </summary>
    public virtual User? User { get; set; }
    public virtual List<Tag> Tag { get; set; } = [];
}
