﻿namespace Backend.Library.Exceptions;

/// <summary>
/// Прикладное исключение
/// </summary>
public class AppException : Exception
{
    /// <summary>
    /// Прикладное исключение с сообщением
    /// </summary>
    /// <param name="message">Сообщение об ошибке</param>
    public AppException(string message) : base(message) { }
}
