﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace Backend.library.Filters;

/// <summary>
/// Протоколирование времени запроса и параметров
/// </summary>
public class DiagnosticAttribute : ActionFilterAttribute
{
    private const string timeVariable = "startTime";
    private static NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();


    /// <summary>
    /// Обработка перед входом в метод действия
    /// </summary>
    /// <param name="context"></param>
    /// <param name="next"></param>
    public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
    {
        var request = context.HttpContext.Request;
        request.Body.Seek(0, SeekOrigin.Begin);
        using StreamReader streamReader = new StreamReader(request.Body);
        string body = await streamReader.ReadToEndAsync();
        if (!string.IsNullOrEmpty(body))
        {
            log.Trace($"{body}");
        }
        request.RouteValues.Add(timeVariable, DateTime.Now);

        await base.OnActionExecutionAsync(context, next);
    }


    /// <summary>
    /// Обработка после входа
    /// </summary>
    /// <param name="context"></param>
    public override void OnActionExecuted(ActionExecutedContext context)
    {
        var request = context.HttpContext.Request;
        DateTime dateTime = (DateTime)request.RouteValues[timeVariable]!;
        TimeSpan elapsed = DateTime.Now - dateTime;
        log.Log(elapsed.TotalMilliseconds > 1000 ? NLog.LogLevel.Warn : NLog.LogLevel.Trace, $"{request.Method}{request.Path} {request.QueryString} : {elapsed.TotalMilliseconds} mc");
    }
}

