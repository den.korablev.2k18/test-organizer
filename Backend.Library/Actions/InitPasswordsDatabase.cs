﻿using Backend.Library.Storage;
using NLog;

namespace Backend.Library.Actions;

public class InitPasswordsDatabase
{
    private readonly DB db;
    private const string filePassword = "10_million_password_list_top_1000000.txt";
    private static Logger log = LogManager.GetCurrentClassLogger();
    public InitPasswordsDatabase(DB db)
    {
        this.db = db; 
    }

    public void InitDatabase()
    {

        try
        {
            using StreamReader reader = new(filePassword);
            while (reader.Peek() >= 0)
            {
                string line = reader.ReadLine();
                if (line == null)
                {
                    db.SaveChanges();
                }
                else
                {
                    PasswordHash passwordHash = new()
                    {
                        PasswordRaw = line,
                        PassowrdHash = PasswordHash.Hash(line)
                    };
                    db.PasswordHashes.Add(passwordHash);
                }
            }
            log.Trace("База с паролями инициализирована");
            db.SaveChanges();
        }
        catch (Exception ex)
        {
            log.Fatal(ex);
        }
    }
}
