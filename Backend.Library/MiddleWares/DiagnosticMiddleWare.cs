﻿using Microsoft.AspNetCore.Http;

namespace Backend.library.MiddleWares;

/// <summary>
/// MiddleWare
/// </summary>
public class DiagnosticMiddleWare
{
    private readonly RequestDelegate next;
    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="requestDelegate">След обработчик в конструкторе вызовов</param>
    public DiagnosticMiddleWare(RequestDelegate requestDelegate) => this.next = requestDelegate;
    /// <summary>
    /// Включение буферизации и передача 
    /// </summary>
    /// <param name="httpContext"></param>
    /// <returns></returns>
    public async Task InvokeAsync(HttpContext httpContext)
    {
        httpContext.Request.EnableBuffering();
        await next(httpContext);
    }
}
