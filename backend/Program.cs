using Backend.Library.Actions;
using Backend.Library.Statics;
using Backend.Library.Storage;
using Backend.Services.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using NLog;
using System.Text;
namespace Backend
{
    public class Program
    {
        private static Logger log = LogManager.GetCurrentClassLogger();
        public static void Main(string[] args)
        {
            try
            {
                using (var db = new Backend.Library.Storage.DB())
                {
                    db.Database.Migrate();
                    if(!db.PasswordHashes.Any())
                    {
                        InitPasswordDatabase(db);
                    }
                }
                log.Info("  ");

                var builder = WebApplication.CreateBuilder(args);

                // Add services to the container.

                builder.Services.AddControllers();

                builder.Services.AddCors(p => p.AddPolicy("corsapp", builder =>
                {
                    builder.WithOrigins("*").AllowAnyMethod().AllowAnyHeader();
                }));
                // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
                builder.Services.AddEndpointsApiExplorer();
                builder.Services.AddSwaggerGen(swagger =>
                {
                    swagger.AddSecurityRequirement(new Microsoft.OpenApi.Models.OpenApiSecurityRequirement
                    {
                        {
                            new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "Bearer"
                                }
                            },
                            new string[]{}
                        }
                    });
                });
                builder.Services.AddAuthentication(option =>
                {
                    option.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    option.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                }).AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = false,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Config.GetString("Jwt:Issuer"),
                        ValidAudience = Config.GetString("Jwt:Audience"),
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Config.GetString("Jwt:Key")))

                    };
                });

                builder.Services.AddDbContext<DB>();
                builder.Services.AddSingleton<UserService>();
                builder.Services.AddSingleton<NotificationService>();
                builder.Services.AddSingleton<TagService>();


                var app = builder.Build();

                app.UseSwagger();
                app.UseSwaggerUI();

                app.UseAuthentication();
                app.UseAuthorization();
                app.UseMiddleware<Backend.library.MiddleWares.DiagnosticMiddleWare>();

                app.UseCors("corsapp");
                app.MapControllers();

                app.Run();

            } catch (Exception ex)
            {
                log.Fatal(ex);
            }
        }

        private static void InitPasswordDatabase(DB dB)
        {
            InitPasswordsDatabase initPasswordsDatabase = new(dB);
            initPasswordsDatabase.InitDatabase();
        }
    }
}
