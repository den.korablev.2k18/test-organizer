﻿using Backend.Library.Storage;
using Backend.Services.Models;
using Backend.Services.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Api.Controllers;

public class UserController : RootController
{
    private readonly DB db;
    /// <summary>
    /// Конструктор контроллера 
    /// </summary>
    /// <param name="db"></param>
    /// 

    private readonly UserService _userService;
    public UserController(DB db, UserService userService)
    {
        this.db = db;
        _userService = userService;
    }
    /// <summary>
    /// Инициализация пользователя
    /// </summary>
    /// <param name="inComeUser"></param>
    /// <returns></returns>
    [HttpPost]
    [AllowAnonymous]
    public ActionResult InizializeUser(InComeUser inComeUser)
    {
        var response = _userService.InizializeUser(inComeUser, db);
        if (response.ErrorMessage == null)
        {
            return Ok(response);
        } else
        {
            return BadRequest(response);
        }
    }
}
