﻿using Backend.Library.Storage;
using Backend.Services.Services;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Api.Controllers;
/// <summary>
/// Контроллер уведомлений
/// </summary>
public class NotificationController : RootController
{
    private readonly DB db;
    private readonly NotificationService _notificationService;
    public NotificationController(DB db, NotificationService notificationService)
    {
        this.db = db; 
        _notificationService = notificationService;
    }
    /// <summary>
    /// Создание уведомления
    /// </summary>
    /// <param name="notification">Объект уведомления</param>
    /// <returns></returns>
    [HttpPost]
    public ActionResult CreateNotification(Notification notification)
    {
        return Ok(_notificationService.Create(notification, db));
    }
}
