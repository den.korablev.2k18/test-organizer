﻿using Backend.Library.Storage;
using Backend.Services.Services;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Api.Controllers;
/// <summary>
/// Контроллер тега
/// </summary>
public class TagController : RootController
{
    private readonly DB db;
    public readonly TagService tagService;
    public TagController(DB db, TagService tagService)
    {
        this.db = db;
        this.tagService = tagService;
    }
    [HttpPost]
    public ActionResult CreateTag(Tag tag)
    {
        return Ok(tagService.Create(tag, db));
    }
}
