﻿using Backend.library.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace Backend.Api.Controllers;

[ApiController]
[Authorize(AuthenticationSchemes = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme)]
[Diagnostic]
[Route("api/[controller]/[action]")]
public class RootController : ControllerBase
{
    protected static Logger log = LogManager.GetCurrentClassLogger();
}
