﻿namespace Backend.Services.Models;

public class InComeUser
{
    /// <summary>
    /// логин
    /// </summary>
    public string Login {  get; set; }
    /// <summary>
    /// Сырой пароль
    /// </summary>
    public string Password { get; set; }
    /// <summary>
    /// Проверка на регистрацию
    /// </summary>
    public bool isRegistration { get; set; }
}
