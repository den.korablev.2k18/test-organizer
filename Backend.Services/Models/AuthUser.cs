﻿namespace Backend.Services.Models;

public class AuthUser
{
    public string? Token { get; set; }
    public Guid? UserId { get; set; }
    public string? ErrorMessage { get; set; }
}
