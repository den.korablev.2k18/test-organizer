﻿namespace Backend.Services.Models;

/// <summary>
/// Универсальный ответ
/// </summary>
public class Response
{
    /// <summary>
    /// Сообщение об ошибке
    /// </summary>
    public string? ErrorMessage { get; set; }
    public string? Token { get; set; }
}

