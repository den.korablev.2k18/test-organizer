﻿using Backend.Library.Storage;

namespace Backend.Services.Services;

public class BaseService
{
    /// <summary>
    /// Шаблонный метод добавления нового объекта
    /// </summary>
    /// <typeparam name="T">Тип данных объекта</typeparam>
    /// <param name="entity">Добавляемый объект</param>
    /// <returns></returns>
    public Guid InsertEntity<T>(T entity, DB db) where T : Entity
    {
        entity.Id = Guid.NewGuid();
        db.Set<T>().Add(entity);
        db.SaveChanges();
        return entity.Id;
    }
}
