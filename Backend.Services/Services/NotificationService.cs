﻿using Backend.Library.Storage;

namespace Backend.Services.Services;

/// <summary>
/// Сервис уведомлений
/// </summary>
public class NotificationService : BaseService
{
    public Guid Create(Notification notification, DB db)
    {
        return InsertEntity<Notification>(notification, db);
    }
}
