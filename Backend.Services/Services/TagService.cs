﻿using Backend.Library.Storage;

namespace Backend.Services.Services;
/// <summary>
/// Сервис тега
/// </summary>
public class TagService : BaseService
{
    public Guid Create(Tag tag, DB dB)
    {
        return InsertEntity<Tag>(tag, dB);
    }
}
