﻿using Backend.Library.Statics;
using Backend.Library.Storage;
using Backend.Services.Models;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
namespace Backend.Services.Services;
/// <summary>
/// Сервис пользователей
/// </summary>
public class UserService
{
    public AuthUser InizializeUser(InComeUser inComeUser, DB db)
    {
        User? user;
        AuthUser authUser = new AuthUser();
        PasswordHash? passwordHash;
        passwordHash = db.PasswordHashes.FirstOrDefault((item) => item.PassowrdHash == GetHash(inComeUser.Password));
        if (passwordHash != null)
        {
            authUser.ErrorMessage = "Ваш пароль не безопасный";
            return authUser;
        }

        if (inComeUser.isRegistration)
        {
            if (db.Users.FirstOrDefault((user) => user.Login == inComeUser.Login) != null)
            {
                authUser.ErrorMessage = "Пользователь с данным логином уже существует";
                return authUser;
            }
            user = new User()
            {
                Login = inComeUser.Login,
                PasswordHash = GetHash(inComeUser.Password)
            };
            db.Users.Add(user);
            db.SaveChanges();
        }
        else
        {
            user = db.Users.FirstOrDefault((item) => item.Login == inComeUser.Login && item.PasswordHash == GetHash(inComeUser.Password));

        }
        if (user == null)
        {
            authUser.ErrorMessage = "Пользователь не найден";
            return authUser;
        }
        var tokenString = GenerateJwtToken(inComeUser.Login);
        authUser.Token = tokenString;
        authUser.UserId = user.Id;
        return authUser;
    }
    /// <summary>
    /// Хеширование пароля
    /// </summary>
    /// <param></param>
    /// <returns></returns>
    private string GetHash(string password)
    {
        using var hasher = MD5.Create();
        string hash = Convert.ToHexString(hasher.ComputeHash(Encoding.UTF8.GetBytes(password)));
        return hash;
    }
    /// <summary>
    /// Генерация JWT 
    /// </summary>
    /// <param name="userName">Логин</param>
    /// <returns></returns>
    private string GenerateJwtToken(string userName)
    {
        var tokenHandler = new JwtSecurityTokenHandler();
        var key = Encoding.ASCII.GetBytes(Config.GetString("Jwt:Key"));
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new[] { new Claim("id", userName) }),
            Expires = DateTime.UtcNow.AddHours(1),
            Issuer = Config.GetString("Jwt:Issuer"),
            Audience = Config.GetString("Jwt:Audience"),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
        };
        var token = tokenHandler.CreateToken(tokenDescriptor);
        return tokenHandler.WriteToken(token);
    }
}
